package pgao.bookmark.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import pgao.bookmark.activity.BaseActivity;
import pgao.bookmark.handler.BookmarkPopupHandler;
import pgao.bookmark.activity.MainActivity;
import pgao.bookmark.PopupMenuBookmark;
import pgao.bookmark.R;
import pgao.bookmark.adapters.BookmarkAdapter;
import pgao.bookmark.handler.ToolbarHandler;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.TagDAO;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.Tag;

/**
 * Created by Xakbash on 21/02/2015.
 */

public class BookmarkFragment extends Fragment implements BookmarkAdapter.OnItemClickListener {

    public static final String TAG = "BookmarkFragment";
    public static final String ARG_TAG_ID = "tag_id";
    public static final int NO_TAG_ID = -1;

    private MainActivity mActivity;
    private BookmarkAdapter mAdapter;

    private OnFragmentInteractionListener mListener;

    //private List<Bookmark> mList;
    //private ActionMode m_actionMode;
    private BookmarkPopupHandler mBookmarkPopupHandler;

    //private String mArgList;
    private int mArgTagId;

    //private ListView mListView;

    public BookmarkFragment() {
    }

    // ON CREATE
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();

        // Arguments
        if (getArguments() != null) {
            mArgTagId = getArguments().getInt(ARG_TAG_ID);
        }

        //mListView = getListView();

        BookmarkDAO mBookmarkDao = new BookmarkDAO(getActivity());
        TagDAO mTagDAO = new TagDAO(getActivity());

        List<Bookmark> bookmarks = null;

        // List with context display
        switch (mActivity.getCurrentView()) {
            case MainActivity.DRAWER_VIEW_FRAG_ALL:
                bookmarks = mBookmarkDao.findAll();
                break;

            case MainActivity.DRAWER_VIEW_FRAG_FAVORITES:
                bookmarks = mBookmarkDao.findFavoritesBookmarks();
                break;

            case MainActivity.VIEW_FRAG_BOOKMARK_TAG:
                Tag tag = mTagDAO.findById(mArgTagId);
                bookmarks = mBookmarkDao.findObjectsForObject(tag);
                mActivity.setTitle(tag.getName());
                break;
        }

        mAdapter = new BookmarkAdapter(mActivity, bookmarks);
        //setListAdapter(mAdapter);
    }

    // ON CREATE VIEW
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bookmark, container, false);
        mActivity.getToolbarHandler().adaptToolbar(R.drawable.ic_menu, ToolbarHandler.NAVIGATION_ICON_DRAWER, R.menu.menu_bookmark, false);
        mAdapter.setOnItemClickListener(this);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(android.R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    // STATIC FUNCTION: NEW INSTANCE
    public static BookmarkFragment newInstance(int tagId) {
        BookmarkFragment fragment = new BookmarkFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_TAG_ID, tagId);
        //args.putString(ARG_LIST, listDisplayed);
        fragment.setArguments(args);

        return fragment;
    }

    // EVENT: ON FINISH EDIT DIALOG
    public void onFinishEditDialog(Bookmark newBookmark, List<Tag> newTags, List<Tag> tagsExisted) {

        // All
        if (mActivity.getCurrentView() == BaseActivity.DRAWER_VIEW_FRAG_ALL)
        {
            if (newBookmark != null)
            {
                mAdapter.add(newBookmark);
            }
        }

        // For tag
        else if (mActivity.getCurrentView() == BaseActivity.VIEW_FRAG_BOOKMARK_TAG)
        {
            if (newTags != null) {
                for (int i = 0; i < tagsExisted.size(); i++) {
                    if (tagsExisted.get(i).getId() == mArgTagId) {
                        mAdapter.add(newBookmark);
                    }
                }
            }
        }
    }

    // LISTENER: ON BOOKMARK CLICK
    public AdapterView.OnItemClickListener bookmark_click_lst = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Bookmark bookmark = (Bookmark) adapterView.getItemAtPosition(i);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(bookmark.getLink()));
            mActivity.getBaseContext().startActivity(intent); // TODO : Régler le prob
        }
    };

   /* public void setEmptyText(CharSequence emptyText) {
        View emptyView = .getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }*/

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(View view, Bookmark bookmark) {
        if (null != mListener) {
            //PictureFile picture = mAdapter.getList().get(position);
            mListener.onFragmentInteraction(bookmark.getId());
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int id);
    }
}

