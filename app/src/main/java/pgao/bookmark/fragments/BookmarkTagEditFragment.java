package pgao.bookmark.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.List;

import pgao.bookmark.activity.MainActivity;
import pgao.bookmark.R;
import pgao.bookmark.handler.ToolbarHandler;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.BookmarkTagDAO;
import pgao.bookmark.models.dao.TagDAO;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.BookmarkTag;
import pgao.bookmark.models.objects.Tag;

/**
 * Created by Xakbash on 22/02/2015.
 */
public class BookmarkTagEditFragment extends Fragment {

    public static final String TAG = "EditLabelsBookmarkFragment";
    public static final String ARG_BOOKMARK_ID = "bookmark_id";

    private MainActivity mActivity;
    private BookmarkTagDAO mBookmarkTagDAO;
    private TagDAO mTagDAO;
    private Bookmark mBookmark;
    private String mStr_tags;

    private EditText mEdt_labels;

    private int mBookmarkId;

    public BookmarkTagEditFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();

        if (getArguments() != null) {
            mBookmarkId = getArguments().getInt(ARG_BOOKMARK_ID);
        }

        mActivity.showTagFragment(mBookmarkId);
        //mTagFragment = TagFragment.newInstance(mActivity, mBookmarkId);
    }

    // ON CREATE VIEW
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         // Views
        View rootView = inflater.inflate(R.layout.fragment_edit_labels_of_bookmark, container, false);
        mActivity.getToolbarHandler().adaptToolbar(R.drawable.abc_ic_ab_back_mtrl_am_alpha, ToolbarHandler.NAVIGATION_ICON_BACK, R.menu.menu_edit_labels_bookmark, false);

        mEdt_labels = (EditText) rootView.findViewById(R.id.edt_edit_labels_ofBookmark);

        BookmarkDAO bookmarkDAO = new BookmarkDAO(mActivity);
        mBookmarkTagDAO = new BookmarkTagDAO(mActivity);
        mTagDAO = new TagDAO(mActivity);

        mBookmark = bookmarkDAO.findById(mBookmarkId);
        List<Tag> tags = mTagDAO.findObjectsForObject(mBookmark);

        StringBuilder strb_tags = new StringBuilder();
        for (int i = 0; i < tags.size(); i++) {
            if (i != 0 && i != tags.size()) {
                strb_tags.append(", ");
            }
            strb_tags.append(tags.get(i).getName());
        }

        mStr_tags = strb_tags.toString();
        mEdt_labels.setText(strb_tags);

        mEdt_labels.setOnEditorActionListener(edt_labels_editorAction_lst);

        return rootView;
    }

    private EditText.OnEditorActionListener edt_labels_editorAction_lst = new EditText.OnEditorActionListener()
    {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (EditorInfo.IME_ACTION_DONE == actionId)
            {
                saveTags();
            }
            // QUAND ADD ET QUAND DELETE TAG ENLEVER DE LIST EN BAS OU AJOUTER
            return false;
        }
    };

    // EVENT: ON ITEM TAG CLICK
    public void onItemTagClick(Tag tag) {
        StringBuilder strb_newTag = new StringBuilder();
        strb_newTag.append(mEdt_labels.getText());

        // If first tag
        if (!mEdt_labels.getText().toString().equals("")) {
            strb_newTag.append(", ");
        }
        strb_newTag.append(tag.getName());
        mEdt_labels.setText(strb_newTag);

       // mTagFragment.getAdapter().remove(tag);
    }

    // STATIC FUNCTION: SHOW FRAGMENT
    public static BookmarkTagEditFragment newInstance(int bookmark_id) {
        BookmarkTagEditFragment fragment = new BookmarkTagEditFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_BOOKMARK_ID, bookmark_id);
        fragment.setArguments(args);

        return fragment;
    }

    // FUNCTION : SAVE TAGS
    public void saveTags()
    {
        String tags = mEdt_labels.getText().toString();

        // Create new tags
        try {
            int nbNewTags = BookmarkAddFragment.createTags(tags, mBookmark, mTagDAO, mBookmarkTagDAO);
           // mActivity.updateCounter(MainActivity.VIEW_DRAWER_LABELS, nbNewTags);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        // Delete relations
        String[] oldTags = mStr_tags.split(",");
        String[] newTags = tags.split(",");

        // Each Old tag
        for (String tag : oldTags) {
            tag = tag.trim();

            boolean already = false;

            // Each New tag
            for (String newTag : newTags)
            {
                newTag = newTag.trim();

                // If new tag is already associed
                if (tag.equals(newTag))
                {
                    already = true;
                }
            }

            if (!already)
            {
                Tag tr = mTagDAO.findByName(tag);
                BookmarkTag bookmarkTag = mBookmarkTagDAO.findByBookmarkTag(mBookmark, tr);
                mBookmarkTagDAO.delete(bookmarkTag);
            }
        }
    }
}