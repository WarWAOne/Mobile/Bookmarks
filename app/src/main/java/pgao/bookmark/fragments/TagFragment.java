package pgao.bookmark.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import pgao.bookmark.DialogRename;
import pgao.bookmark.activity.BaseActivity;
import pgao.bookmark.activity.MainActivity;
import pgao.bookmark.R;
import pgao.bookmark.adapters.TagAdapter;
import pgao.bookmark.handler.ToolbarHandler;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.TagDAO;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.Tag;

public class TagFragment extends Fragment implements TagAdapter.OnItemClickListener {

    public static final String TAG = "TagFragment";

    //public static final String ARG_MODE = "mode"; // Edit or display
    public static final String ARG_BOOKMARK_ID = "bookmark_id"; // id or not id (Bookmark of tag or all tags)

    public static final int NO_BOOKMARK_ID = -1;
    //public static final String MODE_EDIT = "mode_edit";
    //public static final String MODE_DISPLAY = "mode_display";

    //public static final String ARG_LIST_DISPLAYED = "list_displayed";

    private MainActivity mActivity;
    private TagAdapter mAdapter;

    //private String mMode;
    private int mBookmarkId;

    private OnFragmentInteractionListener mListener;

    public TagFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();

        // Args
        if (getArguments() != null) {
            mBookmarkId = getArguments().getInt(ARG_BOOKMARK_ID);
           // mMode = getArguments().getString(ARG_MODE);
        }

        //ListView mListView = getListView();

        TagDAO tagDao = new TagDAO(mActivity);
        List<Tag> l_tags = null;

        if (mActivity.getCurrentView() == BaseActivity.DRAWER_VIEW_FRAG_LABELS) {
            l_tags = tagDao.findAll();
        }
        else if (mActivity.getCurrentView() == BaseActivity.VIEW_FRAG_BOOKMARK_EDIT_TAG) {
            BookmarkDAO bookmarkDAO = new BookmarkDAO(mActivity);
            Bookmark bookmark = bookmarkDAO.findById(mBookmarkId);
            l_tags = tagDao.findTagsNotAssociatedToOneBookmark(bookmark);
        }

        mAdapter = new TagAdapter(mActivity, l_tags);

        /* Adapter
        if (mMode.equals(MODE_DISPLAY)) {
            mAdapter = new TagAdapter(mActivity, l_tags);
        }
        else if (mMode.equals(MODE_EDIT))
        {
            mAdapter = new TagAdapter(mActivity, l_tags);
        }*/

        //setListAdapter(mAdapter);
    }

    // ON CREATE VIEW
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tag, container, false);
        mActivity.getToolbarHandler().adaptToolbar(R.drawable.ic_menu, ToolbarHandler.NAVIGATION_ICON_DRAWER, ToolbarHandler.NO_MENU, false);
        mAdapter.setOnItemClickListener(this); // Toolbar en fonction de view

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(android.R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

       /* SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

            }
        });*/
        return rootView;
    }

    // ON ACTIVITY CREATED
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(View view, Tag tag) {
        if (null != mListener) {
            //PictureFile picture = mAdapter.getList().get(position);
           mListener.onFragmentInteraction(tag.getName());
        }
    }

    public void setEmptyText(CharSequence emptyText) {
        /*View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }*/
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String path);
    }

    /* LISTENER: TAG CLICK
    private ListView.OnItemClickListener tag_click_lst = new ListView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (mListDisplayed.equals(LIST_TAG_BOOKMARK)) {
                BookmarkTagEditFragment frag = (BookmarkTagEditFragment) mActivity.getFragmentByTag(BookmarkTagEditFragment.TAG);
                Tag tag = (Tag) mListView.getItemAtPosition(position);
                frag.onItemTagClick(tag);
            }
            else if (mContextDisplay.equals(CONTEXT_DISPLAY_NORMAL))
            {
                Tag tag = (Tag) mListView.getItemAtPosition(position);
                mActivity.displayView(MainActivity.VIEW_BOOKMARK_TAG, tag.getId());
            }
            else
            {
                BookmarkAddFragment frag = (BookmarkAddFragment) mActivity.getFragmentByTag(BookmarkAddFragment.TAG);
                Tag tag = (Tag) mListView.getItemAtPosition(position);
                frag.onItemTagClick(tag);
            }
        }
    };*/

    /* LISTENER: BTN DELETE TAG CLICK
    private View.OnClickListener btn_delete_click_lst = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showConfirmDialog((TagAdapter.ViewHolder) view.getTag());
        }
    };

    // LISTENER: BTN RENAME TAG CLICK
    private View.OnClickListener btn_rename_click_lst = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TagAdapter.ViewHolder holder = (TagAdapter.ViewHolder) view.getTag();
            DialogRename dialog = new DialogRename(mActivity, holder.tag, mAdapter);
            dialog.show();
        }
    };*/

    /* FUNCTION : SHOW CONFIRM DELETION DIALOG
    private void showConfirmDialog(TagAdapter.ViewHolder holder)
    {
        final Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_confirm);
        dialog.setTitle("Deleting tag");
        TextView tv_confirm = (TextView) dialog.findViewById(R.id.msg_confirm);
        Button btn_confirm_cancel = (Button) dialog.findViewById(R.id.btn_confirm_cancel);
        Button btn_confirm_delete = (Button) dialog.findViewById(R.id.btn_confirm_delete);
        tv_confirm.setText("Are you sure you want to delete the tag \"" + holder.tag.getName() + "\" ?");
        btn_confirm_cancel.setTag(holder);
        btn_confirm_delete.setTag(holder);
        System.out.println(btn_confirm_cancel.getText());

        btn_confirm_delete.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                TagAdapter.ViewHolder holder = (TagAdapter.ViewHolder) v.getTag();
                mAdapter.remove(holder.tag);
                //mActivity.updateCounter(mActivity.VIEW_DRAWER_ALL, -1);
                new TagDAO(mActivity.getBaseContext()).delete(holder.tag);
                dialog.dismiss();
            }
        });

        btn_confirm_cancel.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }*/

    // STATIC FUNCTION: NEW INSTANCE
    public static TagFragment newInstance(final int bookmarkId) {
        TagFragment fragment = new TagFragment();

        Bundle args = new Bundle();
        //args.putString(ARG_MODE, mode);
        args.putInt(ARG_BOOKMARK_ID, bookmarkId);
        fragment.setArguments(args);

        return fragment;
    }

    // GETTERS
    public TagAdapter getAdapter() { return mAdapter; }
   // public String getMode() { return mMode; }
    public void setBookmarkId(int bookmarkId) {  mBookmarkId = bookmarkId; }
}
