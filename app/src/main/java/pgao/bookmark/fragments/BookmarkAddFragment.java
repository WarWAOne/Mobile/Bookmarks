package pgao.bookmark.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pgao.bookmark.activity.MainActivity;
import pgao.bookmark.R;
import pgao.bookmark.handler.ToolbarHandler;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.BookmarkTagDAO;
import pgao.bookmark.models.dao.Dao;
import pgao.bookmark.models.dao.TagDAO;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.BookmarkTag;
import pgao.bookmark.models.objects.Tag;

public class BookmarkAddFragment extends Fragment implements OnEditorActionListener {

    public static final String TAG ="AddBookmarkFragment";

    public static final String ARG_TAG_ID = "tag_id";
    public static final String ARG_BOOKMARK_LINK = "bookmark_link";

    public static final int NO_TAG_ID = -1;
    public static final String NO_BOOKMARK_LINK = "";

    private MainActivity mActivity;
    private BookmarkFragment mFragment;

    private EditText mEdtTitle;
    private EditText mEdtLink;
    private EditText mEdtTags;

    private List<Tag> mTags = new ArrayList<>();
    private List<Tag> mTagsExist = new ArrayList<>();

    private String mBookmarkLink;
    private int mTagId;

	public BookmarkAddFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();

        // Arguments
        if (getArguments() != null) {
            mTagId = getArguments().getInt(ARG_TAG_ID);
            mBookmarkLink = getArguments().getString(ARG_BOOKMARK_LINK);
        }

        //mFragment = (BookmarkFragment) mActivity.getFragmentByTag(BookmarkFragment.TAG);
        //TagFragment.newInstance(mActivity, TagFragment.VIEW_TYPE_DISPLAY, TagFragment.LIST_ALL);
        //mActivity.showTagFragment(TagFragment.NO_BOOKMARK_ID);
        mActivity.showTagFragment(TagFragment.NO_BOOKMARK_ID);
    }

	// ON CREATE VIEW
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_add_bookmark, container, false);
        mActivity.getToolbarHandler().adaptToolbar(R.drawable.abc_ic_ab_back_mtrl_am_alpha, ToolbarHandler.NAVIGATION_ICON_BACK, R.menu.menu_add_bookmark, false);
		
		// Views
        mEdtTitle = (EditText) rootView.findViewById(R.id.edt_newBookmark_title);
        mEdtLink = (EditText) rootView.findViewById(R.id.edt_newBookmark_link);
        mEdtTags = (EditText) rootView.findViewById(R.id.edt_newBookmark_tags);

        if (mTagId != -1) {
            TagDAO tagDao = new TagDAO(mActivity);
            Tag tag = tagDao.findById(mTagId);
            mEdtTags.setText(String.valueOf(tag.getName() + ", "));
        }

        if (!mBookmarkLink.equals("")) {
            mEdtLink.setText(mBookmarkLink);
        }

        mEdtTitle.requestFocus();
        mEdtTags.setOnEditorActionListener(this);

		return rootView;
	}

   /* @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

    }*/

    // LISTENER: ON EDIT TEXT EDITOR ACTION
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

		if (EditorInfo.IME_ACTION_DONE == actionId) {
            try {
                mActivity.getToolbarHandler().handleItemClick(mActivity.getToolbar().getMenu().findItem(R.id.action_add_bookmark));
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
		}
		return false;
	}

    // FUNCTION: ADD BOOKMARK
    public void addBookmark() throws SQLException {

        Bookmark newBookmark;

        // Get contents of EditTexts
        String title = mEdtTitle.getText().toString();
        String link = mEdtLink.getText().toString();
        String tags = mEdtTags.getText().toString();

        // If the inputs are empty
        if ((title.equals("")) || (link.equals("")))
        {
            Toast.makeText(mActivity, R.string.toast_error_empty_inputs, Toast.LENGTH_LONG).show();
        }
        else
        {
            Pattern pattern = Pattern.compile("((https?:/\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_/.0-9#:+?%=&;,]*)?)?)");
            Matcher matcher = pattern.matcher(link);

            //If the url is not valid
            if (!matcher.matches())
            {
                Toast.makeText(mActivity, R.string.toast_error_link, Toast.LENGTH_LONG).show();
            }
            else
            {
                Dao<Bookmark> bookmarkDao = new BookmarkDAO(getActivity());
                TagDAO tagDao = new TagDAO(getActivity());

                // TODO : Get the fav icon
					/*GetSiteIcon task = new GetSiteIcon();
					task.execute(str_link);

					try
					{
					    dw_icon = task.get();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
					catch (ExecutionException e) {
						e.printStackTrace();
					}*/

                int i_icon = R.drawable.ic_bookmark;

                //Create bookmark
                newBookmark = new Bookmark(title.trim(), link.trim(), i_icon,  false, 1);
                bookmarkDao.create(newBookmark);

                List<Bookmark> bookmarks;
                bookmarks = bookmarkDao.findLast();
                newBookmark.setId(bookmarks.get(0).getId());

                // If there is tags
                if (tags.length() > 0)
                {
                    createTags(tags, newBookmark, tagDao, new BookmarkTagDAO(mActivity));
                    //int nbNewTags = createTags(tags, newBookmark, tagDao, new BookmarkTagDAO(mActivity));
                    //mActivity.updateCounter(MainActivity.VIEW_DRAWER_LABELS, nbNewTags);
                }

                mActivity.onFinishEditDialog(newBookmark.getTitle());
                //mFragment.onFinishEditDialog(newBookmark, mTags, mTagsExist);
            }
        }
    }

    // EVENT: ON ITEM TAG CLICK
    public void onItemTagClick(Tag tag) {
        StringBuilder strb_newTag = new StringBuilder();
        strb_newTag.append(mEdtTags.getText());

        // If first tag
        if (!mEdtTags.getText().toString().equals("")) {
            strb_newTag.append(", ");
        }
        strb_newTag.append(tag.getName());
        mEdtTags.setText(strb_newTag);

       // TagFragment tagFrag = (TagFragment) mActivity.getFragmentByTag(TagFragment.TAG);
        //tagFrag.getAdapter().remove(tag);
    }

    public static int createTags(String tagsNames, Bookmark bookmark, TagDAO tagDao, BookmarkTagDAO bookmarkTagDAO) throws SQLException {

        int nbNewTags = 0;

		// Split tags
        String[] tags = tagsNames.split(",");

        // Creating witch tag
        for (String tagName : tags)
        {
            tagName = tagName.trim();
            //tagName = tagName.replaceAll("\\s", "");
            Tag tag = tagDao.findByName(tagName);

            // Tag not exist
            if(tag == null)
            {
                Tag newTag = new Tag(tagName, 1);

                // Create new tag
                if (!newTag.getName().equals("")) {
                    tagDao.create(newTag);
                    BookmarkTag relation = new BookmarkTag(bookmark, newTag);
                    bookmarkTagDAO.create(relation);
                    nbNewTags++;
                }
            }

            // Tag exist
            else
            {
                if (bookmarkTagDAO.findByBookmarkTag(bookmark, tag) == null)
                {
                    BookmarkTag relation = new BookmarkTag(bookmark, tag);
                    bookmarkTagDAO.create(relation);
                }
            }
        }
        return nbNewTags;
    }

    // STATIC FUNCTION: NEW INSTANCE
    public static BookmarkAddFragment newInstance(AppCompatActivity activity, int tag_id,  String bookmark_link) {
        BookmarkAddFragment fragment = new BookmarkAddFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(ARG_TAG_ID, tag_id);
        bundle.putString(ARG_BOOKMARK_LINK, bookmark_link);
        fragment.setArguments(bundle);

        return fragment;
    }
}
