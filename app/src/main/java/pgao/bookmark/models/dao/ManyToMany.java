package pgao.bookmark.models.dao;

import java.sql.SQLException;
import java.util.List;

/** ManyToMany
 *
 * @param <Parent>
 * @param <Child>
 */
public interface ManyToMany<Parent, Child>
{
    // METHODS
    public List<Parent> findObjectsForObject(Child child) throws SQLException;
    //public void createRelation(Parent parent, Child child);
}
