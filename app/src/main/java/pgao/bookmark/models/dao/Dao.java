package pgao.bookmark.models.dao;

import android.content.Context;

import java.sql.SQLException;
import java.util.List;

import pgao.bookmark.Database;

public abstract class Dao<T> {

    protected final Context context;

    // CONSTRUCTOR
    public Dao(Context context)
    {
        this.context = context;
    }

    // METHODS
    public abstract List<T> findAll();
    public abstract T findById(int id);
    public abstract List<T> findLast() throws SQLException;
    public abstract void create(T obj);
    public abstract void update(T obj);
    public abstract void delete(T obj);
    public abstract Database getHelper();
}


