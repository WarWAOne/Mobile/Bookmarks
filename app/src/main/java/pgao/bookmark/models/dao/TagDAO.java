package pgao.bookmark.models.dao;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

import pgao.bookmark.Database;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.BookmarkTag;
import pgao.bookmark.models.objects.Tag;

public class TagDAO extends Dao<Tag> implements ManyToMany<Tag, Bookmark>{

    private Database database;

    private static final String COL_NAME = "name";

    RuntimeExceptionDao<Tag, Integer> tagDao = getHelper().getTag_DAO();
    RuntimeExceptionDao<BookmarkTag, Integer> bookmarkTagDao = getHelper().getBookmarkTag_DAO();

    // CONSTRUCTOR
    public TagDAO(Context context) {
        super(context);
    }

    // CREATE TAG
    @Override
    public void create(Tag obj) {
        tagDao.create(obj);
    }

    // UPDATE TAG
    @Override
    public void update(Tag obj) {
        tagDao.update(obj);
    }

    // DELETE TAG
    @Override
    public void delete(Tag obj) {
        tagDao.delete(obj);
    }

    // FIND ALL TAGS
    @Override
    public List<Tag> findAll()
    {
        return tagDao.queryForAll();
    }

    // FIND TAG BY ID
    @Override
    public Tag findById(int id)
    {
        return tagDao.queryForId(id);
    }

    // FIND BY NAME
    public Tag findByName(String name)
    {
        try {
        QueryBuilder<Tag, Integer> qb = tagDao.queryBuilder();
        qb.where().eq(COL_NAME, name);
        PreparedQuery<Tag> pq = qb.prepare();
        return tagDao.queryForFirst(pq);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // FIND LAST TAG
    @Override
    public List<Tag> findLast() {
        return null;
    }

    // GET TAGS FOR ONE BOOKMARK
    @Override
    public List<Tag> findObjectsForObject(Bookmark bookmark)
    {
        try {
            // Make the Query Builder
            QueryBuilder<BookmarkTag, Integer> bookmarkTagQb = bookmarkTagDao.queryBuilder();
            bookmarkTagQb.selectColumns(BookmarkTag.COL_TAG_ID);

            // Arguments
            SelectArg bookmarkSelectArg = new SelectArg();
            bookmarkTagQb.where().eq(BookmarkTag.COL_BOOKMARK_ID, bookmarkSelectArg);

            // Make the Query Builder Join
            QueryBuilder<Tag, Integer> tagQb = tagDao.queryBuilder();
            tagQb.where().in(Bookmark.COL_ID, bookmarkTagQb);

            // Setting argument
            PreparedQuery<Tag> tagForBookmark = tagQb.prepare();
            tagForBookmark.setArgumentHolderValue(0, bookmark);

            return tagDao.query(tagForBookmark);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // GET TAGS WHO ARE NOT ASSOCIATED WITH THE TAG
    public List<Tag> findTagsNotAssociatedToOneBookmark(Bookmark bookmark)
    {
        try {
            // Make the Query Builder
            QueryBuilder<BookmarkTag, Integer> bookmarkTagQb = bookmarkTagDao.queryBuilder();
            bookmarkTagQb.selectColumns(BookmarkTag.COL_TAG_ID);

            // Arguments
            SelectArg bookmarkSelectArg = new SelectArg();
            bookmarkTagQb.where().eq(BookmarkTag.COL_BOOKMARK_ID, bookmarkSelectArg);

            // Make the Query Builder Join
            QueryBuilder<Tag, Integer> tagQb = tagDao.queryBuilder();
            tagQb.where().notIn(Bookmark.COL_ID, bookmarkTagQb);

            // Setting argument
            PreparedQuery<Tag> tagForBookmark = tagQb.prepare();
            tagForBookmark.setArgumentHolderValue(0, bookmark);

            return tagDao.query(tagForBookmark);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // GET HELPER
    @Override
    public Database getHelper()
    {
        if (database == null)
        {
            database = OpenHelperManager.getHelper(context, Database.class);
        }
        return database;
    }
}
