package pgao.bookmark.models.dao;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;

import java.sql.SQLException;
import java.util.List;

import pgao.bookmark.Database;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.BookmarkTag;
import pgao.bookmark.models.objects.Tag;

public class BookmarkTagDAO extends Dao<BookmarkTag> {

    private RuntimeExceptionDao<BookmarkTag, Integer> bookmarkTagDao = getHelper().getBookmarkTag_DAO();
    RuntimeExceptionDao<Tag, Integer> tagDao = getHelper().getTag_DAO();
    RuntimeExceptionDao<Bookmark, Integer> bookmarkDao = getHelper().getBookmark_DAO();
    private Database database;
    private static final String COL_ID  = "id";

    public BookmarkTagDAO(Context context) {
        super(context);
    }

    // FIND ALL RELATIONS
    @Override
    public List<BookmarkTag> findAll() {
        return bookmarkTagDao.queryForAll();
    }

    // FIND RELATION BY ID
    @Override
    public BookmarkTag findById(int id)
    {
        return bookmarkTagDao.queryForId(id);
    }

    // FIND RELATION BY BOOKMARK TAG
    public BookmarkTag findByBookmarkTag(Bookmark bookmark, Tag tag) {
        try {
            // Make the Query Builder
            QueryBuilder<BookmarkTag, Integer> bookmarkTagQb = bookmarkTagDao.queryBuilder();
            bookmarkTagQb.selectColumns(BookmarkTag.COL_BOOKMARK_ID, BookmarkTag.COL_TAG_ID)
                    .where().eq(BookmarkTag.COL_BOOKMARK_ID, bookmark)
                    .and().eq(BookmarkTag.COL_TAG_ID, tag);

            PreparedQuery<BookmarkTag> query = bookmarkTagQb.prepare();

            return bookmarkTagDao.queryForFirst(query);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // FIND LAST RELATION
    @Override
    public List<BookmarkTag> findLast() throws SQLException {
        return null;
    }

    // CREATE RELATION
    @Override
    public void create(BookmarkTag obj)
    {
      bookmarkTagDao.create(obj);
       // return id;
    }

    // UPDATE RELATION
    @Override
    public void update(BookmarkTag obj)
    {
        bookmarkTagDao.update(obj);
    }

    // DELETE RELATION
    @Override
    public void delete(BookmarkTag obj)
    {
        bookmarkTagDao.delete(obj);
    }

    // GET HELPER
    @Override
    public Database getHelper()
    {
        if (database == null)
        {
            database = OpenHelperManager.getHelper(context, Database.class);
        }
        return database;
    }
}
