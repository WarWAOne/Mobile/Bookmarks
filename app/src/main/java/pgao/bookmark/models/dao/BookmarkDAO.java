package pgao.bookmark.models.dao;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;

import java.sql.SQLException;
import java.util.List;

import pgao.bookmark.Database;
import pgao.bookmark.models.objects.BookmarkTag;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.Tag;

public class BookmarkDAO extends Dao<Bookmark> implements ManyToMany<Bookmark, Tag>{

    private Database mDatabase;
    private final RuntimeExceptionDao<Bookmark, Integer> bookmarkDao = getHelper().getBookmark_DAO();
    private final RuntimeExceptionDao<BookmarkTag, Integer> bookmarkTagDao = getHelper().getBookmarkTag_DAO();

    // CONSTRUCTOR
    public BookmarkDAO(Context context) {
        super(context);
    }

    // CREATE BOOKMARK
    @Override
    public void create(Bookmark obj)
    {
       bookmarkDao.create(obj);
       // return id;
    }

    // UPDATE BOOKMARK
    @Override
    public void update(Bookmark obj)
    {
        bookmarkDao.update(obj);
    }

    // DELETE BOOKMARK
    @Override
    public void delete(Bookmark obj)
    {
        bookmarkDao.delete(obj);
    }

    // FIND ALL BOOKMARKS
    @Override
    public List<Bookmark> findAll()
    {
        return bookmarkDao.queryForAll();
    }

    // FIND BOOKMARK BY ID
    @Override
    public Bookmark findById(int id)
    {
        return bookmarkDao.queryForId(id);
    }

    // FIND LAST BOOKMARK
    @Override
    public List<Bookmark> findLast() {
        try {
            QueryBuilder<Bookmark, Integer> bookmarkQb = bookmarkDao.queryBuilder();
            bookmarkQb.selectColumns(Bookmark.COL_ID).orderBy(Bookmark.COL_ID, false).limit(1L);
            PreparedQuery<Bookmark> lastId = bookmarkQb.prepare();
            return bookmarkDao.query(lastId);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	// FIND FAVORITES BOOKMARKS
	public List<Bookmark> findFavoritesBookmarks() {
        try {
            QueryBuilder<Bookmark, Integer> bookmarkQb = bookmarkDao.queryBuilder();
            bookmarkQb.where().eq(Bookmark.COL_FAVORITE, true);
            PreparedQuery<Bookmark> query = bookmarkQb.prepare();

            return bookmarkDao.query(query);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
	}

    // GET BOOKMARK FOR ONE TAG
    @Override
    public List<Bookmark> findObjectsForObject(Tag otherObject)
    {
        try {
            // Make the Query Builder
            QueryBuilder<BookmarkTag, Integer> bookmarkTagQb = bookmarkTagDao.queryBuilder();
            bookmarkTagQb.selectColumns(BookmarkTag.COL_BOOKMARK_ID);

            // Arguments
            SelectArg tagSelectArg = new SelectArg();
            bookmarkTagQb.where().eq(BookmarkTag.COL_TAG_ID, tagSelectArg);

            // Make the Query Builder Join
            QueryBuilder<Bookmark, Integer> bookmarkQb = bookmarkDao.queryBuilder();
            bookmarkQb.where().in(Tag.COL_ID, bookmarkTagQb);

            // Setting arguments
            PreparedQuery<Bookmark> bookmarksForTag = bookmarkQb.prepare();
            bookmarksForTag.setArgumentHolderValue(0, otherObject);

            return bookmarkDao.query(bookmarksForTag);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // GET HElPER
    @Override
    public Database getHelper()
    {
        if (mDatabase == null)
        {
            mDatabase = OpenHelperManager.getHelper(context, Database.class);
        }
        return mDatabase;
    }

}



