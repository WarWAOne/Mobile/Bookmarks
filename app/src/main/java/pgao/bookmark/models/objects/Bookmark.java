package pgao.bookmark.models.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import android.graphics.drawable.*;

@DatabaseTable(tableName = "Bookmark")
public class Bookmark {

    public final static String COL_ID = "id";
	public final static String COL_TITLE = "title";
	public final static String COL_LINK = "link";
	public final static String COL_FAVORITE = "favorite";
	public final static String COL_USER_ID = "user_id";

    //Fields
    @DatabaseField(generatedId = true, columnName = COL_ID)
    private int id;

    @DatabaseField(columnName = COL_TITLE)
    private String title;

    @DatabaseField(columnName = COL_LINK)
    private String link;
	
	@DatabaseField
	private int icon;

    @DatabaseField(columnName = COL_FAVORITE)
    private boolean favorite;

    @DatabaseField(columnName = COL_USER_ID)
    private int user_id;

    //OrmLite constructor
    Bookmark(){}

    //Constructor
    public Bookmark(String title, String link, int icon,  boolean favorite, int user_id)
    {
        this.title = title;
        this.link = link;
		this.icon = icon;
        this.favorite = favorite;
        this.user_id = user_id;
    }

    public Bookmark(int i) {
    }

    //Constructor with id
    public Bookmark(int id, String title, String link, int icon, boolean favorite, int user_id)
    {
        this.id = id;
        this.title = title;
        this.link = link;
		this.icon = icon;
        this.favorite = favorite;
        this.user_id = user_id;
    }

    //Getters
    public int getId(){
        return this.id;
    }

    public String getTitle(){
        return this.title;
    }

    public String getLink() {
        return this.link;
    }
	
	public int getIcon() {
		return this.icon;
	}

    public int getUser_id(){
        return this.user_id;
    }

    public boolean getFavorite(){
        return this.favorite;
    }

    //Setters
    public void  setId(int id){
        this.id = id;
    }

    public void  setUser_id(int user_id){
        this.user_id = user_id;
    }

    public void setTitle(String title){
        this.title = title;
    }
	
	public void setIcon(int icon) {
		this.icon = icon;
	}

    public void setLink(String link){
        this.link = link;
    }

    public void setFavorite(boolean favorite){
        this.favorite = favorite;
    }
	
	// IS
	
	public boolean isFavorite() {
		return this.favorite;
	}
}
