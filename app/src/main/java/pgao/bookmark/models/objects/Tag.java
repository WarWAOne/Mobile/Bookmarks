package pgao.bookmark.models.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

//Table
@DatabaseTable(tableName = "Tag")
public class Tag {
    public final static String COL_ID = "id";
    public final static String COL_NAME = "name";

    //Fields
    @DatabaseField(generatedId = true, columnName = COL_ID)
    private int id;

    @DatabaseField(columnName = COL_NAME)
    private String name;

    @DatabaseField
    private int user_id;

    //OrmLite constructor
    Tag(){}

    //Constructor
    public Tag(String name, int user_id)
    {
        this.name = name;
        this.user_id = user_id;
    }

    //Constructor with id
    public Tag(int id, String name, int user_id)
    {
        this.id = id;
        this.name = name;
        this.user_id = user_id;
    }

    //Getters
    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public int getUser_id(){
        return this.user_id;
    }

    //Setters
    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}


