package pgao.bookmark.models.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "BookmarkTag")
public class BookmarkTag {

    public final static String COL_ID = "id";
    public final static String COL_BOOKMARK_ID = "bookmark_id";
    public final static String COL_TAG_ID = "tag_id";

    //Fields
    @DatabaseField(generatedId = true, columnName = COL_ID)
    int id;

    @DatabaseField(foreign = true, columnName = COL_BOOKMARK_ID)
    Bookmark bookmark;

    @DatabaseField(foreign = true, columnName = COL_TAG_ID)
    Tag tag;

    // ORM LITE CONSTRUCTOR
    BookmarkTag(){}

    // CONSTRUCTOR
    public BookmarkTag(Bookmark bookmark, Tag tag)
    {
        this.id = id;
        this.bookmark = bookmark;
        this.tag = tag;
    }

    public BookmarkTag(int id, Bookmark bookmark, Tag tag)
    {
        this.id = id;
        this.bookmark = bookmark;
        this.tag = tag;
    }

    // GETTERS
    public int getId(){
        return this.id;
    }

}
