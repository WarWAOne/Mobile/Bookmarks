package pgao.bookmark;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.PopupMenu;
import android.widget.ShareActionProvider;

import pgao.bookmark.activity.BaseActivity;
import pgao.bookmark.activity.MainActivity;
import pgao.bookmark.adapters.BookmarkAdapter;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.objects.Bookmark;

/**
 * Created by Xakbash on 22/02/2015.
 */
public class PopupMenuBookmark extends PopupMenu {

    //private Context m_context;
    private BookmarkAdapter mAdapter;
    private Bookmark mBookmark;
    private BookmarkDAO mBookmarkDAO;
    private MainActivity mActivity;

    public PopupMenuBookmark(Context context, View anchor, BookmarkAdapter adapter, Bookmark bookmark) {
        super(context, anchor);
        //m_context = context;
        mAdapter = adapter;
        mBookmark = bookmark;
        mBookmarkDAO = new BookmarkDAO(context);
        mActivity = (MainActivity) context;

        inflate(R.menu.popup_bookmark);
        setOnMenuItemClickListener(popupMenu_item_click_listener);
    }

    // ON POPUPMENU MENU ITEM CLICK LISTENER
    private PopupMenu.OnMenuItemClickListener popupMenu_item_click_listener = new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId())  {

                case R.id.bookmark_popup_rename:
                     DialogRename dialog = new DialogRename(mActivity, mBookmark, mAdapter);
                     dialog.show();
                    return true;

                case R.id.bookmark_popup_labels:
                    mActivity.setCurrentView(BaseActivity.VIEW_FRAG_BOOKMARK_EDIT_TAG);
                     mActivity.showBookmarkTagEditFragment(mBookmark.getId());
                    return true;

                case R.id.bookmark_popup_share:
                    final ShareActionProvider provider = new ShareActionProvider(mActivity);
                    final Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_TEXT, mBookmark.getLink());
                    provider.setShareIntent(i);
                    item.setActionProvider(provider);
                    return true;

                case R.id.bookmark_popup_delete:   // TODO : Make a snackbar
                    mAdapter.remove(mBookmark);
                    mBookmarkDAO.delete(mBookmark);
                    return true;

                default:
                    return false;
            }
        }
    };
}
