package pgao.bookmark.adapters;

import android.content.Context;
import android.support.v4.view.ViewGroupCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.support.v7.widget.RecyclerView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pgao.bookmark.R;
import pgao.bookmark.activity.BaseActivity;
import pgao.bookmark.activity.MainActivity;
import pgao.bookmark.fragments.TagFragment;
import pgao.bookmark.models.objects.Tag;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder> implements View.OnClickListener {

    public static final String VIEW_TYPE_DISPLAY = "view_type_display";
    public static final String VIEW_TYPE_EDIT = "view_type_edit";

    private MainActivity mActivity;
    private TagFragment mFragment;
    private LayoutInflater mInflater;
    private List<Tag> mTags;

    //private String mViewType;
    private TagFragment mOnItemClickListener;

    //private View.OnClickListener mBtn_delete_click_lst;
    //private View.OnClickListener mBtn_rename_click_lst;

    // private ListView mListView;

    // CONSTRUCTOR
    public TagAdapter(Context context, List<Tag> tags) {
        mActivity = (MainActivity) context;
        mFragment = (TagFragment) mActivity.getSupportFragmentManager().findFragmentByTag(TagFragment.TAG);
        mInflater = LayoutInflater.from(context);
        mTags = tags;

        //mListView = listView;
        //mViewType = viewType;

        /* mTag_click_lst = tag_click_lst;
        mBtn_rename_click_lst = btn_rename_click_lst;
        mBtn_delete_click_lst = btn_delete_click_lst;*/

        /*if (m_context_click == CONTEX_CLICK_NORMAL) {
            m_listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            m_listView.setMultiChoiceModeListener(new MultiChoiceModeBookmark(m_activity.getMenuInflater(), this, nr));
        }*/
       /* if (mTag_click_lst != null) {
            mListView.setOnItemClickListener(mTag_click_lst);*/
        }

    public static class ViewHolder extends RecyclerView.ViewHolder {
         TextView name;
         ImageView btn_delete;
         ImageView btn_rename;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            btn_delete = (ImageView) itemView.findViewById(R.id.btn_delete_tag);
            btn_rename = (ImageView) itemView.findViewById(R.id.btn_rename_tag);
        }
    }

        @Override
        public ViewHolder onCreateViewHolder (ViewGroup parent,int viewType){
            View v;
            if (mActivity.getCurrentView() == BaseActivity.VIEW_FRAG_EDIT_LABELS) {
                v = mInflater.inflate(R.layout.item_tag_edit, parent, false);
            }
            else {
                v = mInflater.inflate(R.layout.item_tag, parent, false);
            }
            v.setOnClickListener(this);
            return new ViewHolder(v);
        }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Tag tag = mTags.get(position);
        holder.name.setText(tag.getName());
        holder.itemView.setTag(tag);
    }

    public void setOnItemClickListener(TagFragment onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Tag tag);
    }

    // GET THE ID OF ITEM
    @Override
    public long getItemId ( int i)
    {
        return i;
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    @Override
    public void onClick (View view){
        if (mOnItemClickListener != null) {

        }
    }

    public void add(Tag tag) {
        mTags.add(tag);
        notifyDataSetChanged();
    }

    public void remove(Tag tag) {
        mTags.remove(tag);
        notifyDataSetChanged();
    }


        /* GET VIEW
        @Override
        public View getView ( int i, View view, ViewGroup viewGroup)
        {
            ViewHolder holder;

            Tag tag = (Tag) getItem(i);

            if (view == null) {
                holder = new ViewHolder();

                if (mViewType.equals(VIEW_TYPE_EDIT)) {
                    view = mInflater.inflate(R.layout.item_tag_edit, null);
                    holder.btn_delete = (ImageView) view.findViewById(R.id.btn_delete_tag);
                    holder.btn_rename = (ImageView) view.findViewById(R.id.btn_rename_tag);
                    holder.btn_delete.setTag(holder);
                    holder.btn_rename.setTag(holder);
                } else if (mViewType.equals(VIEW_TYPE_DISPLAY)) {
                    view = mInflater.inflate(R.layout.item_tag, null);
                }

                holder.tv_name = (TextView) view.findViewById(R.id.tag_name);
                holder.tag = tag;

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            //Add content of the TextView
            holder.tv_name.setText(mList.get(i).getName());

            if (mBtn_delete_click_lst != null) {
                holder.btn_delete.setOnClickListener(mBtn_delete_click_lst);
            }
            if (mBtn_rename_click_lst != null) {
                holder.btn_rename.setOnClickListener(mBtn_rename_click_lst);
            }

            return view;
        }*/
}

    /*
    public boolean isPositionChecked(int position) {
        Boolean result = m_selection.get(position);
        return result == null ? false : result;
    }

    public void setNewSelection(int position, boolean value) {
        m_selection.put(position, value);
        notifyDataSetChanged();
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return m_selection.keySet();
    }

    public void removeSelection(int position) {
        m_selection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        m_selection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }*/

   /* public void addItem(Tag newTag) {
        mList.add(newTag);
        notifyDataSetChanged();
    }

    public void remove(Tag tag) {
        mList.remove(tag);
        notifyDataSetChanged();
    }*/


