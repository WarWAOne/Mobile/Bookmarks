con/*package pgao.bookmark.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


import pgao.bookmark.R;
import pgao.bookmark.models.objects.DrawerItem;

public class DrawerAdapter extends BaseAdapter {

    public static final int TYPE_ITEM = 0;
    public static final int TYPE_USER = 1;

    private List<DrawerItem> m_l_items;
    private LayoutInflater m_inflater;
    private Context m_context;

    public DrawerAdapter(Context context, List<DrawerItem> items)
    {
        m_inflater = LayoutInflater.from(context);
        m_l_items = items;
        m_context = context;
    }*/

    /* GET SIZE
    @Override
    public int getCount()
    {
        return m_l_items.size();
    }

    // GET ONE ITEM
    @Override
    public DrawerItem getItem(int i)
    {
        return m_l_items.get(i);
    }

    // GET THE ID OF ITEM
    @Override
    public long getItemId(int i)
    {
        return i;
    }

    // VIEW HOLDER
    private class ViewHolder
    {
        // DrawerItem
        TextView tv_name;
        ImageView imgv_imgIcon;
        TextView tv_Count;

        // User
        TextView tv_username;
        ImageView imgv_userIcon;
    }

     GET VIEW
    @Override
    public View getView(int i, View view, final ViewGroup viewGroup) {
        final ViewHolder holder;
        int rowType = getItemViewType(i);

        if (view == null) {
            holder = new ViewHolder();
            switch (rowType) {
                case TYPE_USER:
                    view = m_inflater.inflate(R.layout.item_drawer_user, null);
                    holder.tv_username = (TextView) view.findViewById(R.id.itemDrawer_username);
                    holder.imgv_userIcon = (ImageView) view.findViewById(R.id.itemDrawer_userIcon);
                    break;
                case TYPE_ITEM:
                    view = m_inflater.inflate(R.layout.item_drawer, null);
                    holder.tv_name = (TextView) view.findViewById(R.id.item_title);
                    holder.imgv_imgIcon = (ImageView) view.findViewById(R.id.item_icon);
                    holder.tv_Count = (TextView) view.findViewById(R.id.item_counter);
                    break;
            }
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        switch (rowType) {

            case TYPE_USER:
                holder.tv_username.setText(m_l_items.get(i).getTitle());
                //holder.imgv_userIcon.setImageResource();
                break;

            case TYPE_ITEM:
                holder.tv_name.setText(m_l_items.get(i).getTitle());
                holder.imgv_imgIcon.setImageResource(m_l_items.get(i).getIcon());

                if (m_l_items.get(i).getCounterVisibility()) {
                    holder.tv_Count.setText(m_l_items.get(i).getCount());
                }
                else {
                    // hide the counter view
                    holder.tv_Count.setVisibility(View.GONE);
                }
                break;
        }
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;

        // Si Item
        if (!m_l_items.get(position).isSpecialItem()) {
            type = TYPE_ITEM;
        }
        // Si User
        else if (m_l_items.get(position).isSpecialItem()){
            type = TYPE_USER;
        }
        return type;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    public void addItem(DrawerItem item) {
        m_l_items.add(item);
        notifyDataSetChanged();
    }*/

    /*public void addUserItem(Book item) {
        m_l_items.add(item);
        notifyDataSetChanged();
    }*

    / FUNCTION: UPDATE COUNTERS
    public void updateCounter(int drawerItem, int value, boolean isPositive) {
        int counter = Integer.parseInt(getItem(drawerItem).getCount());
        int newCounter;

        if (isPositive) {
            newCounter = counter + value;
        }
        else {
            newCounter = counter - value;
        }

        getItem(drawerItem).setCount(Integer.toString(newCounter));

        if (counter <= 0) {
            getItem(drawerItem).setCounterVisibility(true);
        }
        notifyDataSetChanged();
    }
}*/



