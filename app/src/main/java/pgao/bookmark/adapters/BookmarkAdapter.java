package pgao.bookmark.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import pgao.bookmark.PopupMenuBookmark;
import pgao.bookmark.activity.BaseActivity;
import pgao.bookmark.activity.MainActivity;
import pgao.bookmark.R;
import pgao.bookmark.fragments.BookmarkFragment;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.TagDAO;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.Tag;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.ViewHolder> implements View.OnClickListener {

   // public static final String ARG_BOOKMARK_TITLE = "bookmark_title";
    // public static final String ARG_BOOKMARK_LINK = "bookmark_link";

    private MainActivity mActivity;
    private BookmarkFragment mFragment;
    private LayoutInflater mInflater;
    private List<Bookmark> mBookmarks;

    private OnItemClickListener mOnItemClickListener;
    //private View.OnClickListener mBookmark_popup_click_lst;
    //private View.OnClickListener mBookmark_favorite_icon_click_lst;

    private TextView mContextMenu;

    // CONSTRUCTOR
    public BookmarkAdapter(Context context, List<Bookmark> bookmarks)
    {
        mActivity = (MainActivity) context;
        mFragment = (BookmarkFragment) mActivity.getSupportFragmentManager().findFragmentByTag(BookmarkFragment.TAG);
        mInflater = LayoutInflater.from(context);
        mBookmarks = bookmarks;

       // mBookmark_click_lst = bookmark_click_lst;
        //mBookmark_popup_click_lst = bookmark_popup_click_lst;
        //mBookmark_favorite_icon_click_lst = bookmark_favorite_icon_click_lst;

        //if (mBookmark_click_lst != null) {
          //  listView.setOnItemClickListener(mBookmark_click_lst);
        //}
    }

    public void setOnItemClickListener(BookmarkFragment onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Bookmark bookmark);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView tags;
        TextView link;
        ImageView icon;
        ImageView favoriteIcon;
        ImageView popupMenu;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            tags = (TextView) itemView.findViewById(R.id.tags);
            link = (TextView) itemView.findViewById(R.id.link);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            favoriteIcon = (ImageView) itemView.findViewById(R.id.favorite_icon);
            popupMenu = (ImageView) itemView.findViewById(R.id.popup);
        }
    }

    /*CLASS: VEW HOLDER
    public class ViewHolder
    {
        TextView tv_bookmarkTitle;
        TextView tv_bookmarkTags;
        TextView tv_bookmarkLink;
        ImageView iv_bookmarkIcon;
        ImageView iv_bookmarkFavoriteIcon;
        ImageView tv_popupMenu;
        public Bookmark bookmark;
    }*/

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_bookmark, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Bookmark bookmark = mBookmarks.get(position);

        holder.title.setText(bookmark.getTitle());
        holder.link.setText(bookmark.getLink());

        // Get favorite icon
        if (!bookmark.isFavorite()) {
            Drawable dw_notFavoriteIcon = holder.itemView.getResources().getDrawable(R.drawable.ic_star);
            holder.favoriteIcon.setImageDrawable(dw_notFavoriteIcon);
        }
        else {
            Drawable dw_favoriteIcon = holder.itemView.getResources().getDrawable(R.drawable.ic_yellow_star);
            holder.favoriteIcon.setImageDrawable(dw_favoriteIcon);
        }

        // Get tags of the bookmark
        StringBuffer strb_tags;
        strb_tags = getTags(position);

        holder.tags.setText(strb_tags);

        holder.itemView.setTag(bookmark);

        holder.favoriteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookmark.setFavorite(!bookmark.getFavorite());
                new BookmarkDAO(mActivity.getBaseContext()).update(bookmark);
                if (mActivity.getCurrentView() == BaseActivity.DRAWER_VIEW_FRAG_FAVORITES) {
                    remove(bookmark);
                }
                notifyDataSetChanged();
            }
        });

        holder.popupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenuBookmark popupMenu = new PopupMenuBookmark(mActivity, view, BookmarkAdapter.this, bookmark);
                popupMenu.show();
            }
        });
    }

    // GET THE ID OF ITEM
    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public int getItemCount() {
        return mBookmarks.size();
    }

    @Override
    public void onClick(View view) {
        if (mOnItemClickListener != null) {

        }
    }

    public void add(Bookmark newBookmark) {
        mBookmarks.add(newBookmark);
        notifyDataSetChanged();
    }

    public void remove(Bookmark bookmark) {
        mBookmarks.remove(bookmark);
        notifyDataSetChanged();
    }


/*
    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder holder;
		Bookmark bookmark = mLBookmarks.get(i);

        if (view == null)
        {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.item_bookmark, null);

            holder.bookmark = bookmark;

            // Views
            holder.tv_bookmarkTitle = (TextView) view.findViewById(R.id.bookmark_title);
            holder.tv_bookmarkTags = (TextView) view.findViewById(R.id.bookmark_tags);
            holder.tv_bookmarkLink = (TextView) view.findViewById(R.id.bookmark_link);
            holder.iv_bookmarkIcon = (ImageView) view.findViewById(R.id.bookmark_icon);
            holder.iv_bookmarkFavoriteIcon = (ImageView) view.findViewById(R.id.bookmark_favorite_icon);

            // Set tags
            holder.tv_popupMenu = (ImageView) view.findViewById(R.id.bookmark_popup);
            holder.iv_bookmarkFavoriteIcon.setTag(holder);
            holder.tv_popupMenu.setTag(holder);
            view.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) view.getTag();
        }

        // Get icon
        //Drawable dw_icon = m_context.getResources().getDrawable(bookmark.getIcon());

        // Get favorite icon
        if (!bookmark.getFavorite()) {
            Drawable dw_notFavoriteIcon = view.getResources().getDrawable(R.drawable.ic_notfavorite);
            holder.iv_bookmarkFavoriteIcon.setImageDrawable(dw_notFavoriteIcon);
        }
        else {
            Drawable dw_favoriteIcon = view.getResources().getDrawable(R.drawable.star);
            holder.iv_bookmarkFavoriteIcon.setImageDrawable(dw_favoriteIcon);
        }

        // Get tags of the bookmark
        StringBuffer strb_tags;
        strb_tags = getTags(i);

        // Add contents of TextViews
        holder.tv_bookmarkTitle.setText(holder.bookmark.getTitle());
        holder.tv_bookmarkTags.setText(strb_tags);
        holder.tv_bookmarkLink.setText(holder.bookmark.getLink());
        //holder.iv_bookmarkIcon.setImageDrawable(dw_icon);

        /*if (mBookmark_favorite_icon_click_lst != null)
        {
            holder.iv_bookmarkFavoriteIcon.setOnClickListener(mBookmark_favorite_icon_click_lst);
        }
        if (mBookmark_popup_click_lst != null)
        {
            holder.tv_popupMenu.setOnClickListener(mBookmark_popup_click_lst);
        }*/

       // return view;
    //}

    // FUNCTION: GET TAGS
    private StringBuffer getTags(int i)
    {
        TagDAO tagDao = new TagDAO(mActivity.getBaseContext());
        StringBuffer strb_tags = new StringBuffer();
        List<Tag> l_tags;
        l_tags = tagDao.findObjectsForObject(mBookmarks.get(i));

        //If there is tags
        if (l_tags.size() != 0)
        {
            System.err.println("yeah");
            //Add witch tag to the StringBuffer
            for (int j = 0; j < l_tags.size(); j++)
            {
                strb_tags.append(l_tags.get(j).getName());

                //If the tag is the last we don't put a virgule
                if ((j + 1) != l_tags.size())
                {
                    strb_tags.append(", ");
                }
            }
        }
        return strb_tags;
    }

    /* FUNCTION : SET LISTENERS
    public void setListeners(ListView.OnItemClickListener bookmark_click_lst, View.OnClickListener bookmark_popup_click_lst, View.OnClickListener bookmark_favorite_icon_click_lst)
    {
        mBookmark_click_lst = bookmark_click_lst;
        mBookmark_favorite_icon_click_lst = bookmark_favorite_icon_click_lst;
        mBookmark_popup_click_lst = bookmark_popup_click_lst;
    }*/
}
