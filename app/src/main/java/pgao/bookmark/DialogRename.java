package pgao.bookmark;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pgao.bookmark.adapters.BookmarkAdapter;
import pgao.bookmark.adapters.TagAdapter;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.TagDAO;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.Tag;

/**
 * Created by Xakbash on 22/02/2015.
 */
public class DialogRename extends Dialog {
    private Context m_context;
    private EditText m_edt_rename;

    private RecyclerView.Adapter mAdapter;
    private Object mObject;
    private Bookmark mBookmark;
    private Tag mTag;

    public DialogRename(Context context, Object object, BookmarkAdapter adapter) {
        super(context);
        m_context = context;
        mAdapter = adapter;
        mObject = object;

        setContentView(R.layout.dialog_rename);
        m_edt_rename = (EditText) findViewById(R.id.edt_rename);
        Button btn_cancel = (Button) findViewById(R.id.btn_rename_cancel);
        Button btn_save = (Button) findViewById(R.id.btn_rename_save);

        if (mObject.getClass().getName() == Bookmark.class.getName())
        {
            System.out.println("BOOK");
            mBookmark = (Bookmark) mObject;
            setTitle("Rename Bookmark");
            m_edt_rename.setText(mBookmark.getTitle());
        }
        else if (mObject.getClass().getName() == Tag.class.getName())
        {
            mTag = (Tag) mObject;
            setTitle("Rename Tag");
            m_edt_rename.setText(mTag.getName());
        }

        setCancelable(true);
        setCanceledOnTouchOutside(true);

        btn_cancel.setOnClickListener(btn_cancel_click_lst);
        btn_save.setOnClickListener(btn_save_click_lst);

    }

    private View.OnClickListener btn_cancel_click_lst = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cancel();
        }
    };

    private View.OnClickListener btn_save_click_lst = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!m_edt_rename.getText().toString().equals("") ) {

                if (mObject.getClass().getName() == Bookmark.class.getName())
                {
                    BookmarkDAO bookmarkDAO = new BookmarkDAO(m_context);
                    mBookmark.setTitle(m_edt_rename.getText().toString().trim());
                    bookmarkDAO.update(mBookmark);

                    ((BookmarkAdapter) mAdapter).notifyDataSetChanged();
                }
                else if (mObject.getClass().getName() == Tag.class.getName())
                {
                    TagDAO tagDAO = new TagDAO(m_context);
                    mTag.setName(m_edt_rename.getText().toString().trim());
                    tagDAO.update(mTag);

                    ((TagAdapter) mAdapter).notifyDataSetChanged();
                }
                dismiss();
            }
            else {
                Toast.makeText(m_context, "Please enter a name", Toast.LENGTH_SHORT).show();
            }

        }
    };
}
