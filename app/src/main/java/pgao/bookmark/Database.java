package pgao.bookmark;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.sql.SQLException;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.table.TableUtils;

import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.BookmarkTag;
import pgao.bookmark.models.objects.Tag;

public class Database extends OrmLiteSqliteOpenHelper{

    protected static final int VERSION = 1;
    protected static final String NAME = "Appbookmark.db";

    private Dao<Bookmark, Integer> bookmarkDAO = null;
    private Dao<Tag, Integer> tagDAO = null;
    private Dao<BookmarkTag, Integer> bookmarkTagDAO = null;

    private RuntimeExceptionDao<Bookmark, Integer> bookmarkRuntimeDAO = null;
    private RuntimeExceptionDao<Tag, Integer> tagRuntimeDAO = null;
    private RuntimeExceptionDao<BookmarkTag, Integer> bookmarkTagRuntimeDAO = null;

    // CONSTRUCTOR
    public Database(Context context)
    {
        super(context, NAME, null, VERSION);
    }

    // ON CREATE
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource)
    {
        // Trying to create the Bookmark table
        try
        {
            TableUtils.createTable(connectionSource, Bookmark.class);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

        // Trying to create the Tag table
        try
        {
            TableUtils.createTable(connectionSource, Tag.class);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

        // Trying to create BookmarkTag table
        try
        {
            TableUtils.createTable(connectionSource, BookmarkTag.class);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    // ON UPGRADE
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion)
    {
       // Trying to drop the Bookmark table
        try
        {
            TableUtils.dropTable(connectionSource, Bookmark.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

        // Trying to drop the Tag table
        try
        {
            TableUtils.dropTable(connectionSource, Tag.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

        // Trying to drop the BookmarkTag table
        try
        {
            TableUtils.dropTable(connectionSource, BookmarkTag.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }


    // RUNTIME EXCEPTION DAO OF BOOKMARK TABLE
    public RuntimeExceptionDao<Bookmark, Integer> getBookmark_DAO()
    {
        if (bookmarkRuntimeDAO == null)
        {
            bookmarkRuntimeDAO = getRuntimeExceptionDao(Bookmark.class);
        }
        return bookmarkRuntimeDAO;
    }

    // RUNTIME EXCEPTION DAO OF TAG TABLE
    public RuntimeExceptionDao<Tag, Integer> getTag_DAO()
    {
        if (tagRuntimeDAO == null)
        {
            tagRuntimeDAO = getRuntimeExceptionDao(Tag.class);
        }
        return tagRuntimeDAO;
    }

    // RUNTIME EXCEPTION DAO OF BOOKMARK TAG TABLE
    public RuntimeExceptionDao<BookmarkTag, Integer> getBookmarkTag_DAO()
    {
        if (bookmarkTagRuntimeDAO == null)
        {
            bookmarkTagRuntimeDAO = getRuntimeExceptionDao(BookmarkTag.class);
        }
        return bookmarkTagRuntimeDAO;
    }


    // DAO OF BOOKMARK TABLE
    public Dao<Bookmark, Integer> getBookmarkDAO() throws SQLException
    {
        if (bookmarkDAO == null)
        {
            bookmarkDAO = getDao(Bookmark.class);
        }
        return bookmarkDAO;
    }

    // DAO OF TAG TABLE
    public Dao<Tag, Integer> getTagDAO() throws SQLException
    {
        if (tagDAO == null)
        {
            tagDAO = getDao(Tag.class);
        }
        return tagDAO;
    }

    // DAO OF BOOKMARK TAG TABLE
    public Dao<BookmarkTag, Integer> getBookmarkTagDAO() throws SQLException
    {
        if (bookmarkTagDAO == null)
        {
            bookmarkTagDAO = getDao(BookmarkTag.class);
        }
        return bookmarkTagDAO;
    }

    // CLOSE
    @Override
    public void close()
    {
        super.close();
        bookmarkDAO = null;
        tagDAO = null;
        bookmarkTagDAO = null;
        bookmarkRuntimeDAO = null;
        tagRuntimeDAO = null;
        bookmarkTagRuntimeDAO = null;
    }
}









