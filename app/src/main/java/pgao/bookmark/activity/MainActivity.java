package pgao.bookmark.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pgao.bookmark.R;
import pgao.bookmark.activity.BaseActivity;
import pgao.bookmark.Database;
import pgao.bookmark.handler.ToolbarHandler;
import pgao.bookmark.fragments.BookmarkAddFragment;
import pgao.bookmark.fragments.BookmarkFragment;
import pgao.bookmark.fragments.BookmarkTagEditFragment;
import pgao.bookmark.fragments.TagFragment;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.TagDAO;

public class MainActivity extends AppCompatActivity implements BookmarkFragment.OnFragmentInteractionListener, TagFragment.OnFragmentInteractionListener {

    public static final int TAB_ALL = 0;
    public static final int TAB_FAVORITES = 1;
    public static final int TAB_TAGS = 2;

    private LayoutInflater mInflater;

    public FloatingActionButton mBtnFloat;
    private TabLayout mTabLayout;

    private Toolbar mToolbar;
    private Toolbar mBotToolbar;

    private Database mDatabase;
    private ToolbarHandler mToolbarHandler;
    private CharSequence mTitle;
    protected String mFragment;

    private DrawerLayout mDrawerLayout;
    protected NavigationView mDrawerList;

    protected boolean isDrawerOpen;
    private MenuItem mCurrentDrawerItem;
    private int mCurrentView;

    // ON CREATE
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mBotToolbar = new Toolbar(this);
        mBotToolbar = (Toolbar) findViewById(R.id.toolbar_bot);

        mSectionsPagerAdapter = new MusicListPagerAdapter(getSupportFragmentManager());

        mBtnFloat = (FloatingActionButton) this.findViewById(R.id.btn_float);
        mBtnFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBookmarkAddFragment(BookmarkAddFragment.NO_TAG_ID, BookmarkAddFragment.NO_BOOKMARK_LINK);
            }
        });

        // Fragments
        if (savedInstanceState != null) {
            mFragment = savedInstanceState.getString("fragment");
        }
        else {
            mFragment = getIntent().getStringExtra("fragment");
        }

        // Default displaying
        if (savedInstanceState == null) {
            goToNavDrawerItem(DRAWER_VIEW_FRAG_ALL);
        }
    }

    // ON SAVE INSTANCE STATE
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("fragment", mFragment != null ? mFragment :"");
        super.onSaveInstanceState(outState);
    }

    // FUNCTION: GET STARTER INTENT
    private void getStarterIntent() {
        Intent intent = MainActivity.this.getIntent();
        Uri data = intent.getData();
        // TODO : S'occuper du starter intent !
        if (data != null) {
            System.out.println("Pas null");
            if (intent.getType().equals("text/plain")) {
                //AddBookmarkDialogFragment.show(MainActivity.this, 0, data.toString());
                System.out.println("The intent starter is -> " + data.toString());
            }
        }
    }

    //@Override
    //public boolean onSearchRequested() {
      //  Bundle appData = new Bundle();
       // appData.putBoolean(SearchResultsActivity.JARGON, true);
        //startSearch(null, false, appData, false);
        //return true;
    //}

   // @Override public boolean onSearchRequested() {
     //   return super.onSearchRequested();
    //}

    // EVENT: ON FINISH EDIT DIALOG
    public void onFinishEditDialog(String bookmarkTitle)
    {
        Toast.makeText(this, "The bookmark " + bookmarkTitle + " has been added.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFragmentInteraction(int id) {

    }

    @Override
    public void onFragmentInteraction(String path) {

    }

}









