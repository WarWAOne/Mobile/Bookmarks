package pgao.bookmark.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

import pgao.bookmark.Database;
import pgao.bookmark.R;
import pgao.bookmark.fragments.BookmarkAddFragment;
import pgao.bookmark.fragments.BookmarkTagEditFragment;
import pgao.bookmark.handler.ToolbarHandler;
import pgao.bookmark.fragments.BookmarkFragment;
import pgao.bookmark.fragments.TagFragment;
import pgao.bookmark.models.objects.Bookmark;

/**
 * Created by Xakbash on 28/04/2015.
 */
public class BaseActivity extends AppCompatActivity {

    /* VIEWS
    public static final int DRAWER_VIEW_FRAG_ALL = R.id.drawer_item_all;
    public static final int DRAWER_VIEW_FRAG_LABELS= R.id.drawer_item_labels;
    public static final int DRAWER_VIEW_FRAG_FAVORITES = R.id.drawer_item_favorites;

    public static final int VIEW_FRAG_BOOKMARK_TAG = 0;
    public static final int VIEW_FRAG_EDIT_LABELS= 1; // TODO: May be one view because same frag
    public static final int VIEW_FRAG_BOOKMARK_EDIT_TAG = 2;

    public static final int VIEW_ACTIVITY_SEARCH = 4;
    public static final int VIEW_ACTIVITY_ADD_EDIT_BOOKMARK = 5; // TODO : May be replace by frag
    public static final int VIEW_ACTIVITY_LOGIN = 6; // TODO : May don't put here
    public static final int ACTIVITY_MAIN = 7; //*/

    private Database mDatabase;
    private ToolbarHandler mToolbarHandler;
    private CharSequence mTitle;
    protected String mFragment;

    private DrawerLayout mDrawerLayout;
    protected NavigationView mDrawerList;

    protected boolean isDrawerOpen;
    private MenuItem mCurrentDrawerItem;
    private int mCurrentView;

    // TOOLBAR
    protected final void initToolbar() {
        mTitle = getTitle();
        mToolbarHandler = new ToolbarHandler(this, toolbar_itemClick_lst, toolbar_navIcon_click_lst);
    }

    protected void setupNavDrawer()
    {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (NavigationView) findViewById(R.id.drawer_list);
        mDrawerList.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                goToNavDrawerItem(menuItem.getItemId());
                return true;
            }
        });
        //mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                // TODO : hide keyboard
                isDrawerOpen = false;
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                isDrawerOpen = true;
                invalidateOptionsMenu();
            }
        });
    }

    public void goToNavDrawerItem(int item)
    {
        setSelectedNavDrawerItem(item);

        switch (item) {
            case DRAWER_VIEW_FRAG_ALL:
                setCurrentView(DRAWER_VIEW_FRAG_ALL);
                mFragment = showBookmarkFragment(BookmarkFragment.NO_TAG_ID);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                break;

            case DRAWER_VIEW_FRAG_LABELS:
                setCurrentView(DRAWER_VIEW_FRAG_LABELS);
                mFragment = showTagFragment(TagFragment.NO_BOOKMARK_ID);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                break;

            case DRAWER_VIEW_FRAG_FAVORITES:
                setCurrentView(DRAWER_VIEW_FRAG_FAVORITES);
                 mFragment = showBookmarkFragment(BookmarkFragment.NO_TAG_ID);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                break;
        }
        closeNavDrawer();
    }

   public void goToActivity (Class activity) {

        if (activity.equals(EditBookmarkActivity.class)) {

        }
        Intent intent = new Intent(this, activity);    // TODO : Enlever les transitions !
        startActivity(intent);
      // setCurrentView();

       // mActivity.finish();
    }

    private void setSelectedNavDrawerItem(int itemId)
    {
        setCurrentDrawerItem(mDrawerList.getMenu().findItem(itemId));
        setTitle(getCurrentDrawerItem().getTitle());
        getCurrentDrawerItem().setChecked(true);
    }

    public Fragment getFragmentByTag(String fragmentTag)
    {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        return frag;
    }

    public String showBookmarkFragment(int tagId) {
        BookmarkFragment fragment = BookmarkFragment.newInstance(tagId);
        final FragmentManager fm = this.getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();

        // TODO : Mieu gérer les backstacks
        if (getCurrentView() == DRAWER_VIEW_FRAG_ALL) {
            ft.addToBackStack("all");
        }
        else if (getCurrentView() == DRAWER_VIEW_FRAG_FAVORITES)
        {
            ft.addToBackStack("favorite");
        }
        else if (getCurrentView() == VIEW_FRAG_BOOKMARK_TAG)
        {
            ft.addToBackStack("label");
        }

        ft.replace(R.id.container, fragment, BookmarkFragment.TAG);
        ft.commit();
        return BookmarkFragment.TAG;
    }

    public String showTagFragment(int bookmarkId) {
        TagFragment fragment = TagFragment.newInstance(bookmarkId);
        final android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        final android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();

        switch (getCurrentView()) {
            case DRAWER_VIEW_FRAG_LABELS:
                ft.replace(R.id.container, fragment, TagFragment.TAG);
                break;
            case VIEW_FRAG_ADD_BOOKMARK:
                ft.replace(R.id.add_bookmark_container, fragment, TagFragment.TAG);
                break;
            case VIEW_FRAG_BOOKMARK_EDIT_TAG:
                ft.replace(R.id.edit_labels_bookmark_container, fragment, TagFragment.TAG);
                break;
        }

        ft.addToBackStack("label");
        ft.commit();
        return TagFragment.TAG;
    }

    public String showBookmarkAddFragment(int tagID, String link) {
        BookmarkAddFragment fragment = BookmarkAddFragment.newInstance(this, tagID, link);
       final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        ft.addToBackStack("all");
        ft.replace(R.id.container, fragment, BookmarkAddFragment.TAG);
        ft.commit();
        return BookmarkAddFragment.TAG;
    }

    public String showBookmarkTagEditFragment(int bookmarkId) {
        BookmarkTagEditFragment fragment = BookmarkTagEditFragment.newInstance(bookmarkId);
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, fragment, BookmarkTagEditFragment.TAG);
        ft.addToBackStack("all");
        ft.commit();
        return BookmarkTagEditFragment.TAG;
    }

    public Toolbar.OnClickListener toolbar_navIcon_click_lst = new Toolbar.OnClickListener() {
        @Override
        public void onClick(View v) {
            mToolbarHandler.handleNavigationIcon(mToolbarHandler.getNavigationIconClick());
        }
    };

    public Toolbar.OnMenuItemClickListener toolbar_itemClick_lst = new Toolbar.OnMenuItemClickListener()
    {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                return mToolbarHandler.handleItemClick(menuItem);
            }
            catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    };

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getToolbar().setTitle(mTitle);
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen) {
            closeNavDrawer();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Database
        if (mDatabase != null) {
            OpenHelperManager.releaseHelper();
            mDatabase = null;
        }
    }

    public void openNavDrawer () {
        if (mDrawerLayout != null) {
            mDrawerLayout.openDrawer(mDrawerList);
        }
    }

    public void closeNavDrawer () {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // GETTERS
    public Toolbar getToolbar() {
        return mToolbarHandler.getToolbar();
    }

    public ToolbarHandler getToolbarHandler() {
        return mToolbarHandler;
    }

    public MenuItem getCurrentDrawerItem () {
        return mCurrentDrawerItem;
    }

    public int getCurrentView () {
        return mCurrentView;
    }

    public boolean isDrawerOpen() {
        return isDrawerOpen;
    }

    // SETTERS
    protected void setCurrentDrawerItem (MenuItem item) {
        mCurrentDrawerItem = item;
    }

    public void setCurrentView (int view) {
        mCurrentView = view;
    }

}
