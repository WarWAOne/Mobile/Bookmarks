package pgao.bookmark.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pgao.bookmark.R;
import pgao.bookmark.fragments.BookmarkFragment;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.dao.BookmarkTagDAO;
import pgao.bookmark.models.dao.Dao;
import pgao.bookmark.models.dao.TagDAO;
import pgao.bookmark.models.objects.Bookmark;
import pgao.bookmark.models.objects.BookmarkTag;
import pgao.bookmark.models.objects.Tag;

public class AddEditActivity extends AppCompatActivity {

    public static final String ARG_TAG_ID = "tag_id";
    public static final String ARG_BOOKMARK_LINK = "bookmark_link";

    public static final int NO_TAG_ID = -1;
    public static final String NO_BOOKMARK_LINK = "";

    private BookmarkFragment mFragment;

    private EditText mEdtTitle;
    private EditText mEdtLink;
    private EditText mEdtTags;

    private List<Tag> mTags = new ArrayList<>();
    private List<Tag> mTagsExist = new ArrayList<>();

    private String mBookmarkLink;
    private int mTagId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mEdtTitle = (EditText) this.findViewById(R.id.edt_newBookmark_title);
        mEdtLink = (EditText) this.findViewById(R.id.edt_newBookmark_link);
        mEdtTags = (EditText) this.findViewById(R.id.edt_newBookmark_tags);

        if (mTagId != -1) {
            TagDAO tagDao = new TagDAO(this);
            Tag tag = tagDao.findById(mTagId);
            mEdtTags.setText(String.valueOf(tag.getName() + ", "));
        }

        if (!mBookmarkLink.equals("")) {
            mEdtLink.setText(mBookmarkLink);
        }

        mEdtTitle.requestFocus();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    // FUNCTION: ADD BOOKMARK
    public void addBookmark() throws SQLException {

        Bookmark newBookmark;

        // Get contents of EditTexts
        String title = mEdtTitle.getText().toString();
        String link = mEdtLink.getText().toString();
        String tags = mEdtTags.getText().toString();

        // If the inputs are empty
        if ((title.equals("")) || (link.equals("")))
        {
            Toast.makeText(this, R.string.toast_error_empty_inputs, Toast.LENGTH_LONG).show();
        }
        else
        {
            Pattern pattern = Pattern.compile("((https?:/\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_/.0-9#:+?%=&;,]*)?)?)");
            Matcher matcher = pattern.matcher(link);

            //If the url is not valid
            if (!matcher.matches())
            {
                Toast.makeText(this, R.string.toast_error_link, Toast.LENGTH_LONG).show();
            }
            else
            {
                Dao<Bookmark> bookmarkDao = new BookmarkDAO(this);
                TagDAO tagDao = new TagDAO(this);

                // TODO : Get the fav icon
					/*GetSiteIcon task = new GetSiteIcon();
					task.execute(str_link);

					try
					{
					    dw_icon = task.get();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
					catch (ExecutionException e) {
						e.printStackTrace();
					}*/

                int i_icon = R.drawable.ic_bookmark;

                //Create bookmark
                newBookmark = new Bookmark(title.trim(), link.trim(), i_icon,  false, 1);
                bookmarkDao.create(newBookmark);

                List<Bookmark> bookmarks;
                bookmarks = bookmarkDao.findLast();
                newBookmark.setId(bookmarks.get(0).getId());

                // If there is tags
                if (tags.length() > 0)
                {
                    createTags(tags, newBookmark, tagDao, new BookmarkTagDAO(this));
                    //int nbNewTags = createTags(tags, newBookmark, tagDao, new BookmarkTagDAO(mActivity));
                    //mActivity.updateCounter(MainActivity.VIEW_DRAWER_LABELS, nbNewTags);
                }

                //this.onFinishEditDialog(newBookmark.getTitle());
                //mFragment.onFinishEditDialog(newBookmark, mTags, mTagsExist);
            }
        }
    }

    // EVENT: ON ITEM TAG CLICK
    public void onItemTagClick(Tag tag) {
        StringBuilder strb_newTag = new StringBuilder();
        strb_newTag.append(mEdtTags.getText());

        // If first tag
        if (!mEdtTags.getText().toString().equals("")) {
            strb_newTag.append(", ");
        }
        strb_newTag.append(tag.getName());
        mEdtTags.setText(strb_newTag);

        // TagFragment tagFrag = (TagFragment) mActivity.getFragmentByTag(TagFragment.TAG);
        //tagFrag.getAdapter().remove(tag);
    }

    public static int createTags(String tagsNames, Bookmark bookmark, TagDAO tagDao, BookmarkTagDAO bookmarkTagDAO) throws SQLException {

        int nbNewTags = 0;

        // Split tags
        String[] tags = tagsNames.split(",");

        // Creating witch tag
        for (String tagName : tags)
        {
            tagName = tagName.trim();
            //tagName = tagName.replaceAll("\\s", "");
            Tag tag = tagDao.findByName(tagName);

            // Tag not exist
            if(tag == null)
            {
                Tag newTag = new Tag(tagName, 1);

                // Create new tag
                if (!newTag.getName().equals("")) {
                    tagDao.create(newTag);
                    BookmarkTag relation = new BookmarkTag(bookmark, newTag);
                    bookmarkTagDAO.create(relation);
                    nbNewTags++;
                }
            }

            // Tag exist
            else
            {
                if (bookmarkTagDAO.findByBookmarkTag(bookmark, tag) == null)
                {
                    BookmarkTag relation = new BookmarkTag(bookmark, tag);
                    bookmarkTagDAO.create(relation);
                }
            }
        }
        return nbNewTags;
    }

}
