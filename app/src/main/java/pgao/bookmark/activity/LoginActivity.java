package pgao.bookmark.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.net.URL;

import pgao.bookmark.R;

/**
 * Created by Xakbash on 20/02/2015.
 */
public class LoginActivity extends BaseActivity {

    private static final String URL = "http://www.loikg.me/";

    private EditText m_edt_username;
    private EditText m_edt_password;
    private Button m_btn_connection;
    private ProgressDialog m_progressDialog;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Progress Bar
        m_progressDialog = new ProgressDialog(this);
        m_progressDialog.setMessage(getResources().getString(R.string.connection_progress));
        m_progressDialog.setIndeterminate(true);
        m_progressDialog.setCancelable(false);

        // Views
        m_edt_username = (EditText) findViewById(R.id.username);
        m_edt_password = (EditText) findViewById(R.id.password);
        m_btn_connection = (Button) findViewById(R.id.btn_connection);

        m_btn_connection.setOnClickListener(btn_connection_click_listener);
    }

    // LISTENER: BTN CONNECTION CLICK
    private View.OnClickListener btn_connection_click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!m_edt_username.equals("") && !m_edt_password.equals("")) {
                m_progressDialog.show();

                String username = m_edt_username.getText().toString();
                String password = m_edt_password.getText().toString();
            }
            else {

            }
        }
    };

    // FUNCTION: LOG
    private void log(final String username, final String password) {
       // final String pw = md5(password);

        Thread thread = new Thread() {
            public void run() {
                // Looper.prepare();
                // TODO : Connection to the server
                    if (!isOnline())
                    {
                        Toast.makeText(LoginActivity.this, "Please check your connection", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        try
                        {
                            URL url = new URL(URL);
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            urlConnection.setRequestMethod("GET");
                        }
                        catch (java.io.IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
            }
        };
    }

    // FUNCTION : isOnline
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
