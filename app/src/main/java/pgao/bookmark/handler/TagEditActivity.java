/*package pgao.bookmark.Handlers;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

import pgao.bookmark.Database;
import pgao.bookmark.R;
import pgao.bookmark.fragments.TagFragment;

public class TagEditActivity extends ActionBarActivity {

    private Database mDatabase;
    private ToolbarHandler mToolbarHandler;
    private TagFragment mTagFragment;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_edit);

        mTagFragment = (TagFragment) getSupportFragmentManager().findFragmentById(R.id.tag_edit_fragment);

        mTagFragment.setBookmarkId(-1);
        mTagFragment.setListDisplayed(TagFragment.LIST_ALL);
        mTagFragment.setViewType(TagFragment.VIEW_TYPE_EDIT);

        mToolbarHandler = new ToolbarHandler(new Toolbar(this), this, toolbar_itemClick_lst, toolbar_navIcon_click_lst);
        mToolbarHandler.init();
        mToolbarHandler.adaptToolbar(ToolbarHandler.TOOLBAR_TAG_EDIT);
        setTitle("Edit labels");
    }

    // LISTENER: TOOLBAR NAVIGATION ICON CLICK
    public Toolbar.OnClickListener toolbar_navIcon_click_lst = new Toolbar.OnClickListener() {
        @Override
        public void onClick(View v) {
            mToolbarHandler.handleNavigationIcon(ToolbarHandler.TOOLBAR_TAG_EDIT, null, null);
        }
    };

    // LISTENER: TOOLBAR ITEMS CLICK
    public Toolbar.OnMenuItemClickListener toolbar_itemClick_lst = new Toolbar.OnMenuItemClickListener()
    {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                return mToolbarHandler.handleItemClick(menuItem);
            }
            catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
    };

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getToolbar().setTitle(mTitle);
    }

    // ON DESTROY
    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Database
        if (mDatabase != null) {
            OpenHelperManager.releaseHelper();
            mDatabase = null;
        }
    }

    public ToolbarHandler getToolbarHandler() { return mToolbarHandler; }
    public Toolbar getToolbar() {
        return mToolbarHandler.getToolbar();
    }
}*/
