package pgao.bookmark.handler;

import android.app.Activity;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ShareActionProvider;

import pgao.bookmark.activity.EditBookmarkActivity;
import pgao.bookmark.R;
import pgao.bookmark.adapters.BookmarkAdapter;
import pgao.bookmark.models.dao.BookmarkDAO;
import pgao.bookmark.models.objects.Bookmark;

/**
 * Created by Xakbash on 20/02/2015.
 */
public class BookmarkPopupHandler {
    private Activity m_activity;
    private BookmarkAdapter m_adapter;
    private BookmarkDAO m_bookmarkDao;
    private int m;

    public BookmarkPopupHandler(Activity activity, BookmarkAdapter adapter, BookmarkDAO bookmarkDao) {
        m_activity = activity;
        m_adapter = adapter;
        m_bookmarkDao = bookmarkDao;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = m_activity.getMenuInflater();
        inflater.inflate(R.menu.popup_bookmark, menu);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        int io;
        if (info != null) {
            io = info.position;
        }
        else {
            io = this.m;
        }

       // Bookmark bookmark = (Bookmark) m_adapter.getItem(io);
      // menu.setHeaderTitle(bookmark.getTitle());
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int io;
        if (info != null) {
            io = info.position;
        }
        else {
            io = this.m;
        }

       // Bookmark edit_bookmark = (Bookmark) m_adapter.getItem(io);

        switch (item.getItemId()) {
            case R.id.bookmark_popup_rename:
                Intent intent = new Intent();
               // intent.putExtra(EditBookmarkActivity.EXTRA_BOOKMARK_ID, edit_bookmark.getId());
                m_activity.startActivity(intent);
                return true;

            case R.id.bookmark_popup_share:
                final ShareActionProvider provider = new ShareActionProvider(m_activity);
                final Intent i = new Intent(Intent.ACTION_SEND);

                i.setType("text/plain");
                //i.putExtra(BookmarkAdapter.EXTRA_BOOKMARK_TITLE, edit_bookmark.getTitle());
                //i.putExtra(BookmarkAdapter.EXTRA_BOOKMARK_LINK, edit_bookmark.getLink());
                provider.setShareIntent(i);
                item.setActionProvider(provider);

                return true;

            /*case R.id.context_favorite:
                //m_bookmark.setFavorite(!m_bookmark.getFavorite());
                //m_bookmarkDao.update(m_bookmark);
                return true;*/

            case R.id.bookmark_popup_delete:
                //m_bookmarkDao.delete(edit_bookmark);
                //m_adapter.remove(edit_bookmark);
                //updateCounter(0, 1, false);
                return true;

            default:
                return false;
                 // return super.onContextItemSelected(item);
        }
    }
}
