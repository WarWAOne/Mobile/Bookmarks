package pgao.bookmark.handler;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.sql.SQLException;

import pgao.bookmark.activity.BaseActivity;
import pgao.bookmark.R;
import pgao.bookmark.SearchActivity;
import pgao.bookmark.fragments.BookmarkAddFragment;
import pgao.bookmark.fragments.TagFragment;

import android.support.design.widget.AppBarLayout;

/**
 * Created by Xakbash on 18/02/2015.
 */
public class ToolbarHandler {

    public static final int NO_MENU = -1;
    public static final int NAVIGATION_ICON_DRAWER = 0;
    public static final int NAVIGATION_ICON_BACK = 1;

    /*public static final String TOOLBAR_DRAWER_ALL = "toolbar_drawer_all";
    public static final String TOOLBAR_DRAWER_FAVORITES = "toolbar_drawer_favorites";
    public static final String TOOLBAR_DRAWER_LABELS = "toolbar_drawer_labels";
    public static final String TOOLBAR_ADD_BOOKMARK = "toolbar_add_bookmark";
    public static final String TOOLBAR_TAG_EDIT= "toolbar_tag_edit";
    public static final String TOOLBAR_EDIT_BOOKMARK_TAG = "toolbar_edit_bookmark_tag";
    public static final String TOOLBAR_BOOKMARK_TAG = "toolbar_bookmark_tag";*/

    private Toolbar mToolbar;
    private AppBarLayout mAppBarLayout;
    private BaseActivity mActivity;

    private Toolbar.OnMenuItemClickListener mHandlerItemClick;
    private Toolbar.OnClickListener mHandlerNavIconClick;

    private boolean isTransparent;
    private int mNavigationIconClick;

    public ToolbarHandler(Context context, Toolbar.OnMenuItemClickListener handlerItemClick, Toolbar.OnClickListener handlerNavIconClick) {
        mActivity = (BaseActivity) context;
        mToolbar = new Toolbar(context);
        mHandlerItemClick = handlerItemClick;
        mHandlerNavIconClick = handlerNavIconClick;
        isTransparent = false;
        initToolbar();
    }

    public void initToolbar () {
        mToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(mActivity.getResources().getColor(R.color.accent));
        mToolbar.setBackgroundColor(mActivity.getResources().getColor(R.color.primary));
        mToolbar.setOnMenuItemClickListener(mHandlerItemClick);
        mToolbar.setNavigationOnClickListener(mHandlerNavIconClick);
        //mAppBarLayout = (AppBarLayout) mActivity.findViewById(R.id.appBarLayout);
    }

    // TOOLBAR ITEM CLICK LISTENER
    public boolean handleItemClick (MenuItem menuItem) throws SQLException {

        switch (menuItem.getItemId()) {

            case R.id.action_edit_labels_bookmark_save:
                //BookmarkTagEditFragment fragBTE = (BookmarkTagEditFragment) mActivity.getSupportFragmentManager().findFragmentByTag(BookmarkTagEditFragment.TAG);
                //fragBTE.saveTags();
                //mActivity.displayView(MainActivity.VIEW_DRAWER_ALL); // TODO : ou favorites ou bookmark for tag ! Peut etre backstack
                return true;

            case R.id.action_edit_labels:
                mActivity.setCurrentView(BaseActivity.VIEW_FRAG_EDIT_LABELS);
                mActivity.showTagFragment(TagFragment.NO_BOOKMARK_ID);
                return true;

            case R.id.action_add_bookmark:
                BookmarkAddFragment fragBA = (BookmarkAddFragment) mActivity.getFragmentByTag(BookmarkAddFragment.TAG);
                fragBA.addBookmark();
               // mActivity.goToNavDrawerItem(BaseActivity.DRAWER_VIEW_FRAG_ALL);
                //mActivity.displayView(MainActivity.VIEW_DRAWER_ALL); // TODO : ou favorites ou bookmark for tag ! Peut etre backstack
            return true;

            case R.id.action_search:
                Intent intent = new Intent(mActivity, SearchActivity.class);    // TODO : Enlever les transitions !
                mActivity.startActivity(intent); // TODO : utiliser la function goToActivity
                mActivity.finish();
                return true;

            default:
                return false;
        }
    }

    public void adaptToolbar (int navigationIcon, int navigationIconClick, int menu, boolean isTransparent)
    {
        if (mToolbar.getMenu() != null) {
            mToolbar.getMenu().clear();
        }
        if (menu != NO_MENU) {
            mToolbar.inflateMenu(menu);
        }

        mToolbar.setNavigationIcon(navigationIcon);
        //setTransparency(isTransparent);
        mNavigationIconClick = navigationIconClick;
    }

    /* FUNCTION : ADAPT TOOLBAR
    public void adaptToolbar (String toolbarConfig)
    {
        if (mToolbar.getMenu() != null) {
            mToolbar.getMenu().clear();
        }

        switch (toolbarConfig)
        {
            // Drawer views
            case TOOLBAR_DRAWER_ALL:
                mToolbar.inflateMenu(R.menu.bookmark);
                mToolbar.setNavigationIcon(R.drawable.ic_menu);
                handleSearchWidget();
                break;

            case TOOLBAR_DRAWER_FAVORITES:
                mToolbar.inflateMenu(R.menu.bookmark);
                mToolbar.setNavigationIcon(R.drawable.ic_menu);
                handleSearchWidget();
                break;

            case TOOLBAR_DRAWER_LABELS:
                mToolbar.inflateMenu(R.menu.tag);
                mToolbar.setNavigationIcon(R.drawable.ic_menu);
                break;

            // Simple views
            case TOOLBAR_EDIT_BOOKMARK_TAG:
                mToolbar.inflateMenu(R.menu.edit_labels_bookmark);
                mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
                break;

            case TOOLBAR_ADD_BOOKMARK:
                mToolbar.inflateMenu(R.menu.add_bookmark);
                mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
                break;

            case TOOLBAR_TAG_EDIT:
                mToolbar.setNavigationIcon(R.drawable.ic_check);
                break;

            case TOOLBAR_BOOKMARK_TAG:
                mToolbar.inflateMenu(R.menu.bookmark);
                mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
                handleSearchWidget();
                break;
        }
    }*/

    /* LISTENER: TOOLBAR NAVIGATION ICON CLICK
    public void handleNavigationIcon (int actualView) {

        if (actualView == MainActivity.VIEW_DRAWER_ALL || actualView == MainActivity.VIEW_DRAWER_FAVORITES || actualView == MainActivity.VIEW_DRAWER_LABELS)
        {
            handleDrawerButton();
        }
        else
        {
             handleBackButton(actualView);
        }
    }*/

    public void handleNavigationIcon (int navigationIconClick) {
        switch (navigationIconClick) {
            case NAVIGATION_ICON_DRAWER:
                handleDrawerButton();
                break;

            case NAVIGATION_ICON_BACK:
                mActivity.onBackPressed();
                break;
        }
    }

    /* FUNCTION : HANDLE BACK BUTTON
    public void handleBackButton (int actualView)
    {
        // TODO : s'occuper des backstacks
        switch (actualView)
        {
            case MainActivity.VIEW_ADD_BOOKMARK:
                mActivity.displayView(MainActivity.VIEW_DRAWER_ALL);
                break;

            case MainActivity.VIEW_EDIT_TAGS:
                mActivity.displayView(MainActivity.VIEW_DRAWER_LABELS);
                break;

            case MainActivity.VIEW_EDIT_TAGS_BOOKMARK:
                mActivity.displayView(MainActivity.VIEW_DRAWER_ALL);
                break;

            case MainActivity.VIEW_BOOKMARK_TAG:
                mActivity.displayView(MainActivity.VIEW_DRAWER_LABELS);
                break;
        }
      //mActivity.getSupportFragmentManager().popBackStack();
    }*/

    // FUNCTION : HANDLE DRAWER BUTTON
    public void handleDrawerButton()
    {
        if (!mActivity.isDrawerOpen())
        {
            mActivity.openNavDrawer();
        }
    }

    // FUNCTION : HANDLE SEARCH WIDGET
    public void handleSearchWidget()
    {
        // TODO : Adapter pour Search Activity !
        //SearchManager searchManager = (SearchManager) mActivity.getSystemService(Context.SEARCH_SERVICE);
        //SearchView searchView = (SearchView) MenuItemCompat.getActionView(getToolbar().getMenu().findItem(R.id.action_search));
       // searchView.setSearchableInfo(searchManager.getSearchableInfo(mActivity.getComponentName()));
    }

    public Toolbar getToolbar()
    {
        return mToolbar;
    }

    public boolean isTransparent() {
        return isTransparent;
    }

   /* public void setTransparency(boolean isTransparent) {
        if (!isTransparent) {
            mToolbar.getBackground().setAlpha(255);
            mAppBarLayout.getBackground().setAlpha(255);
        }
        else {
            mToolbar.getBackground().setAlpha(0);
            mAppBarLayout.getBackground().setAlpha(0);
        }
    }*/

    public int getNavigationIconClick() {
        return mNavigationIconClick;
    }
}

