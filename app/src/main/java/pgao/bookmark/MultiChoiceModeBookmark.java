package pgao.bookmark;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;import java.lang.Override;

import pgao.bookmark.adapters.TagAdapter;

/**
 * Created by Xakbash on 24/02/2015.
 */
public class MultiChoiceModeBookmark implements AbsListView.MultiChoiceModeListener {

    public MultiChoiceModeBookmark(MenuInflater menuInflater, TagAdapter adapter, int nr) {
        m_menuInflater = menuInflater;
        m_adapter = adapter;
        this.nr = nr;
    }

    private MenuInflater m_menuInflater;
    private TagAdapter m_adapter;
    private int nr;

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
        /*if (checked) {
            nr++;
            m_adapter.setNewSelection(position, checked);
        } else {
            nr--;
            m_adapter.removeSelection(position);
        }
        mode.setTitle(nr + " selected");*/
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
       // m_menuInflater.inflate(R.menu.bookmark, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }
}
